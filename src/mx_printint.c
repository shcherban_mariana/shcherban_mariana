#include "header.h"

void mx_printint(int n) {
    long num = n;
    char arr[100];
    if (num == 0)
        mx_printchar(n + '0');
    if (num < 0) {
        num *= -1;
        mx_printchar('-');
    }
    int i = 0;
    while (num != 0) {
        arr[i++] = (num % 10) + '0';
        num /= 10;
    }
    for (int j = i - 1; j >= 0; j--) {
        mx_printchar(arr[j]);
    }
}
