#ifndef HEADER_H
#define HEADER_H

#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>

bool mx_isdigit(int c);
bool mx_isspace(char c);
void mx_printchar(char c);
void mx_printerr(const char *s);
void mx_printint(int n);
void mx_printstr(const char *s);
int mx_atoi(const char *str);
char *mx_itoa(int number);
double mx_pow(double n, unsigned int pow);
int mx_strlen(const char *s);
char *mx_strnew(const int size);
void mx_str_reverse(char *s);
void mx_swap_char(char *s1, char *s2);
char *mx_strcpy(char *dst, const char *src);
char *mx_strtrim(const char *str);

void mx_check(char *argv[], char *operand1, char *operation, char *operand2, char *result);
void mx_operations(char *operand1, char *operation, char *operand2, char *result);

#endif
